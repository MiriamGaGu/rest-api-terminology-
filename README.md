# REST API Terminology 

Fork and clone this repository. Then, push to github with all bad answers deleted and only the correct answers showing.

**1. What does it mean REST?**
Representational State Transfer.
Is an architectural style for developing web services. REST is popular due to its simplicity and the fact that it builds upon existing systems and features of the internet's HTTP in order to achieve its objectives, as opposed to creating new standards, frameworks and technologies.
 
These means REST can be used over nearly any protocol, it usually takes advantage of HTTP when used for Web APIs. This means that developers do not need to install libraries or additional software in order to take advantage of a REST API design. REST API Design was defined by Dr. Roy Fielding in his 2000 doctorate dissertation. It is notable for its incredible layer of flexibility. Since data is not tied to methods and resources, REST has the ability to handle multiple types of calls, return different data formats and even change structurally with the correct implementation of hypermedia.

It is a client and server model so there must be client and server in the architecture.



**2. Who coined the REST term?**
It was coined by Roy Fielding in 2000.



**3. In which protocol REST is based?**
WEB Protocole HTTP


**4. What are the main building blocks of a REST Architecture?**

URI Resourses
Representations
Server Client
Hypermedia
Web Service

**5. Identifying a resource is easy; you know how to access it and you even know how to request for a specific format. Since REST is using HTTP protocol as a standing point, there are some actions related to resources: CRUD operations.**

Complete the below table:+


|HTTP Verb|  Proposed Action|
|---------|-----------------------|
|GET      | READ Get info   |
|POST     | Creates a new resource|
|PUT      | Update info  |
|DELETE   | Remove a resource |

**6. Status Code**

Another interesting standard that REST can benefit from when based on HTTP is the usage of HTTP status codes.

+ What’s is a status code?

The values of the numeric status code to HTTP requests are as follows. The data sections of messages Error, Forward and redirection responses may be used to contain human-readable diagnostic information.


+ Explain with your own words, the meaning of next codes:

|Status Code|Description|
|-----------|----------------|
|404        | Client Error   |
|200        | Success/OK     |
|500        |Server error    | 

            
**7. Status Code are grouped in five sets.**

Write what are their meaning.

|Group|Description     |
|-----|----------------|
|1XX  | Informacional  | 
|2XX  | Success        | 
|3XX  | Redirection    |
|4XX  | Error          |
|5XX  | Server Error   |

**8. HTTP Status Codes and Their Related Interpretation**

There are the most common status codes in HTTP responses. Please, fill with the required description.

|Status Code|    Meaning        |
|-----------|-------------------|
|200|  OK                       |
|201|  Creted                   |
|204|  NO Content			    |
|301|  Move Permanently         |
|400|  Bad Request              |
|401|  Unauthorized             |
|403|  Forbidden                |
|404|  Not Found                |
|405|  Method not Allowed       |
|500|  Internal Server Error    |
 
###### [To see the full list of HTTP status codes and their meaning, please refer to the RFC of HTTP 1.1](http://tools.ietf.org/html/rfc7231#section-6)

**9. How are called the points of contact between all client apps and the API?**

Endpoints.- A website uses a URL address to make a call to a server and pull up a webpage in a browser. APIs also facilitate calls to a server, but they execute it more simply. They connect the web, allowing developers, applications, and sites to tap into databases and services (or, assets)—much like open-source software. APIs do this by acting like a universal converter plug offering a standard set of instructions.

**10. The following is a good example or bad example of a named access point? And why?**

_meant to list the books in a bookstore_

+ `GET /books/action1`
It's a bad practice but it's not describing what it is.


**11. Uniform Interface**



To solve this problem, you can apply the REST style to the endpoints, and thanks to HTTP, you also have verbs to indicate actions.
The resourse must be on the endpoint, in these case is books

|Old Style                 |  REST Style                |
|--------------------------|----------------------------|
|`/getAllBooks`            | GET books/                 |     
|`/submitNewBook`          | POST books                 |       
|`/updateAuthor`           | PUT authors/:id            |  
|`/getBooksAuthors`        | GET books/:id/authors      |
|`/getNumberOfBooksOnStock`| GET books/                 |
|`/addNewImageToBook`      | POST books/:id/image       |
|`/getBooksImages`         | GET books/:id/image            |
|`/addCoverImage`          | POST books/:id/cover       |
|`/listBookCovers`         | GET books/:id/             |

**12. What JSON does it mean?**

Javascript Object Notation


**13. Anatomy of a `REQUEST`**

Make a `curl` request to _GitHub API_

```sh
$ curl -X GET 'https://api.github.com/users --head'
```

According to the responded request, answer what does it mean the next parts from the handler:

+ _`Content-Type`_. which format is the code
+ _`Status`_. The status of the code
+ _`Date`_. 
+ _`Content-Length`_. Lenght of the content

HTTP/1.1 200 OK
Server: GitHub.com
Date: Tue, 22 Jan 2019 18:12:01 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 2165
Status: 200 OK
X-RateLimit-Limit: 60
X-RateLimit-Remaining: 37
X-RateLimit-Reset: 1548183461
Cache-Control: public, max-age=60, s-maxage=60
Vary: Accept
ETag: "7dc470913f1fe9bb6c7355b50a0737bc"
X-GitHub-Media-Type: github.v3; format=json
Access-Control-Expose-Headers: ETag, Link, Location, Retry-After, X-GitHub-OTP, X-RateLimit-Limit, X-RateLimit-Remaining, X-RateLimit-Reset, X-OAuth-Scopes, X-Accepted-OAuth-Scopes, X-Poll-Interval, X-GitHub-Media-Type
Access-Control-Allow-Origin: *
Strict-Transport-Security: max-age=31536000; includeSubdomains; preload
X-Frame-Options: deny
X-Content-Type-Options: nosniff
X-XSS-Protection: 1; mode=block
Referrer-Policy: origin-when-cross-origin, strict-origin-when-cross-origin
Content-Security-Policy: default-src 'none'
X-GitHub-Request-Id: C99F:125B:18EE8E2:375ACD7:5C475CF1


###### If response is not showing those parts, ask to google how to print them in console.

```sh
# 
```
